FROM image-mirror-prod-registry.cloud.duke.edu/library/ubuntu:22.04

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Openshift Users Group' \
      architecture='x86_64' \
      summary='ubuntu base image, tweaked' \
      description='Base image for Ubuntu stable with some test bits added' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/oit-ssi-systems/ci-testing'

LABEL version='1.0' \
      release='1'

RUN apt-get update && \
    apt-get install -y \
      curl \
      dnsutils \
      httpie \
      iputils-ping \
      jq \
      libnss-unknown \
      netcat \
      rsync \
      socat

RUN mkdir /.httpie && chgrp -R 0 /.httpie && chmod -R g+rwX /.httpie

ENTRYPOINT ["/bin/bash", "-c", "trap : TERM INT; sleep infinity & wait"]
